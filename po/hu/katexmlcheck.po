# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
#
# Kristof Kiszel <ulysses@fsf.hu>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-15 00:59+0000\n"
"PO-Revision-Date: 2022-01-25 10:16+0100\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.07.70\n"

#: plugin_katexmlcheck.cpp:94
#, fuzzy, kde-format
#| msgid "XML Checker Output"
msgid "XML Check"
msgstr "XML-ellenőrző kimenet"

#: plugin_katexmlcheck.cpp:99
#, kde-format
msgid "Validate XML"
msgstr "XML validálása"

#: plugin_katexmlcheck.cpp:132
#, kde-format
msgid "Validate process crashed"
msgstr ""

#: plugin_katexmlcheck.cpp:132 plugin_katexmlcheck.cpp:150
#: plugin_katexmlcheck.cpp:210 plugin_katexmlcheck.cpp:235
#: plugin_katexmlcheck.cpp:255 plugin_katexmlcheck.cpp:343
#, fuzzy, kde-format
#| msgid "XML Checker Output"
msgid "XMLCheck"
msgstr "XML-ellenőrző kimenet"

#: plugin_katexmlcheck.cpp:146
#, kde-format
msgid "No DOCTYPE found, will only check well-formedness."
msgstr ""

#: plugin_katexmlcheck.cpp:148
#, kde-format
msgctxt "%1 refers to the XML DTD"
msgid "'%1' not found, will only check well-formedness."
msgstr ""

#: plugin_katexmlcheck.cpp:234
#, kde-format
msgid "<b>Error:</b> Could not create temporary file '%1'."
msgstr "<b>Hiba:</b> Nem sikerült létrehozni a(z) „%1” ideiglenes fájlt."

#: plugin_katexmlcheck.cpp:253
#, kde-format
msgid ""
"<b>Error:</b> Failed to find xmllint. Please make sure that xmllint is "
"installed. It is part of libxml2."
msgstr ""
"<b>Hiba:</b> Az xmllint nem található. Ellenőrizze, hogy az xmllint "
"telepítve van-e. A libxml2 része."

#: plugin_katexmlcheck.cpp:341
#, kde-format
msgid ""
"<b>Error:</b> Failed to execute xmllint. Please make sure that xmllint is "
"installed. It is part of libxml2."
msgstr ""
"<b>Hiba:</b> Nem sikerült az xmllint futtatása. Ellenőrizze, hogy az xmllint "
"telepítve van-e. A libxml2 része."

#. i18n: ectx: Menu (xml)
#: ui.rc:6
#, kde-format
msgid "&XML"
msgstr "&XML"

#, fuzzy
#~| msgid "XML Checker Output"
#~ msgid "XML Checker"
#~ msgstr "XML-ellenőrző kimenet"

#~ msgid "#"
#~ msgstr "#"

#~ msgid "Line"
#~ msgstr "Sor"

#~ msgid "Column"
#~ msgstr "Oszlop"

#~ msgid "Message"
#~ msgstr "Üzenet"

#~ msgid "Kate XML check"
#~ msgstr "Kate XML ellenőrzés"
