# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
#
# Kishore G <kishore96@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-23 01:00+0000\n"
"PO-Revision-Date: 2023-01-24 21:32+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:250
#, kde-format
msgid "Filter..."
msgstr "வடிகட்டு..."

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:98 lspclientconfigpage.cpp:103
#: lspclientpluginview.cpp:490 lspclientpluginview.cpp:646 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr ""

#: lspclientconfigpage.cpp:213
#, kde-format
msgid "No JSON data to validate."
msgstr "சரிபார்ப்பதற்கு எந்த JSON தரவும் இல்லை"

#: lspclientconfigpage.cpp:222
#, kde-format
msgid "JSON data is valid."
msgstr "JSON முறையானது."

#: lspclientconfigpage.cpp:224
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "JSON முறையற்றது: JSON object இல்லை"

#: lspclientconfigpage.cpp:227
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "JSON முறையற்றது: %1"

#: lspclientconfigpage.cpp:275
#, kde-format
msgid "Delete selected entries"
msgstr "தேர்ந்தெடுக்கப்பட்டுள்ள பதிவுகளை நீக்கு"

#: lspclientconfigpage.cpp:280
#, kde-format
msgid "Delete all entries"
msgstr "அனைத்துப் பதிவுகளையும் நீக்கு"

#: lspclientplugin.cpp:223
#, kde-format
msgid "LSP server start requested"
msgstr "LSP சேவையகத் துவக்கம் கோரப்பட்டது"

#: lspclientplugin.cpp:226
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""
"LSP சேவையகத்தைத் துவக்க வேண்டுமா?<br><br>முழுக் கட்டளை பின்வருவது:<br><br><b>%1</"
"b><br><br>LSP செருகுநிரலின் அமைப்புப்பக்கத்தில் இதை நீங்கள் எப்போது வேண்டுமானாலும் "
"மாற்றியமைக்கலாம்."

#: lspclientplugin.cpp:239
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""

#: lspclientpluginview.cpp:518
#, kde-format
msgid "Go to Definition"
msgstr ""

#: lspclientpluginview.cpp:520
#, kde-format
msgid "Go to Declaration"
msgstr ""

#: lspclientpluginview.cpp:522
#, kde-format
msgid "Go to Type Definition"
msgstr ""

#: lspclientpluginview.cpp:524
#, kde-format
msgid "Find References"
msgstr ""

#: lspclientpluginview.cpp:527
#, kde-format
msgid "Find Implementations"
msgstr ""

#: lspclientpluginview.cpp:529
#, kde-format
msgid "Highlight"
msgstr "முன்னிலைப்படுத்து"

#: lspclientpluginview.cpp:531
#, kde-format
msgid "Symbol Info"
msgstr ""

#: lspclientpluginview.cpp:533
#, kde-format
msgid "Search and Go to Symbol"
msgstr ""

#: lspclientpluginview.cpp:538
#, kde-format
msgid "Format"
msgstr "வடிவமை"

#: lspclientpluginview.cpp:541
#, kde-format
msgid "Rename"
msgstr "மறுபெயரிடு"

#: lspclientpluginview.cpp:545
#, kde-format
msgid "Expand Selection"
msgstr "தேர்வை விரிவுபடுத்து"

#: lspclientpluginview.cpp:548
#, kde-format
msgid "Shrink Selection"
msgstr "தேர்வைச் சுருக்கு"

#: lspclientpluginview.cpp:552
#, kde-format
msgid "Switch Source Header"
msgstr ""

#: lspclientpluginview.cpp:555
#, kde-format
msgid "Expand Macro"
msgstr ""

#: lspclientpluginview.cpp:557
#, kde-format
msgid "Code Action"
msgstr ""

#: lspclientpluginview.cpp:572
#, kde-format
msgid "Show selected completion documentation"
msgstr ""

#: lspclientpluginview.cpp:575
#, kde-format
msgid "Enable signature help with auto completion"
msgstr ""

#: lspclientpluginview.cpp:578
#, kde-format
msgid "Include declaration in references"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:581 lspconfigwidget.ui:96
#, kde-format
msgid "Add parentheses upon function completion"
msgstr ""

#: lspclientpluginview.cpp:584
#, kde-format
msgid "Show hover information"
msgstr "சுட்டியை மேல்வைக்கும்போது விவரங்களைக் காட்டு"

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:587 lspconfigwidget.ui:47
#, kde-format
msgid "Format on typing"
msgstr "தட்டச்சிடும்போதே வடிவமை"

#: lspclientpluginview.cpp:590
#, kde-format
msgid "Incremental document synchronization"
msgstr ""

#: lspclientpluginview.cpp:593
#, kde-format
msgid "Highlight goto location"
msgstr ""

#: lspclientpluginview.cpp:602
#, kde-format
msgid "Show Inlay Hints"
msgstr ""

#: lspclientpluginview.cpp:606
#, kde-format
msgid "Show Diagnostics Notifications"
msgstr ""

#: lspclientpluginview.cpp:611
#, kde-format
msgid "Show Messages"
msgstr "தகவல்களைக் காட்டு"

#: lspclientpluginview.cpp:616
#, kde-format
msgid "Server Memory Usage"
msgstr "சேவையக நினைவுப் பயன்பாட்டு"

#: lspclientpluginview.cpp:620
#, kde-format
msgid "Close All Dynamic Reference Tabs"
msgstr ""

#: lspclientpluginview.cpp:622
#, kde-format
msgid "Restart LSP Server"
msgstr "LSP சேவையகத்தை மீள்துவக்கு"

#: lspclientpluginview.cpp:624
#, kde-format
msgid "Restart All LSP Servers"
msgstr "அனைத்து LSP சேவையகங்களையும் மீள்துவக்கு"

#: lspclientpluginview.cpp:635
#, kde-format
msgid "Go To"
msgstr "இங்கு செல்"

#: lspclientpluginview.cpp:662
#, kde-format
msgid "More options"
msgstr "மேலும் விருப்பங்கள்"

#: lspclientpluginview.cpp:710
#, kde-format
msgid "LSP"
msgstr "LSP"

#: lspclientpluginview.cpp:875 lspclientsymbolview.cpp:285
#, kde-format
msgid "Expand All"
msgstr "அனைத்தையும் விரிவாக்கு"

#: lspclientpluginview.cpp:876 lspclientsymbolview.cpp:286
#, kde-format
msgid "Collapse All"
msgstr "அனைத்தையும் சுருக்கு"

#: lspclientpluginview.cpp:1083
#, kde-format
msgid "RangeHighLight"
msgstr ""

#: lspclientpluginview.cpp:1407
#, kde-format
msgid "Line: %1: "
msgstr "வரி: %1: "

#: lspclientpluginview.cpp:1559 lspclientpluginview.cpp:1911
#: lspclientpluginview.cpp:2032
#, kde-format
msgid "No results"
msgstr "முடிவுகள் இல்லை"

#: lspclientpluginview.cpp:1618
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr ""

#: lspclientpluginview.cpp:1624
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr ""

#: lspclientpluginview.cpp:1630
#, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr ""

#: lspclientpluginview.cpp:1636
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr ""

#: lspclientpluginview.cpp:1648
#, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr ""

#: lspclientpluginview.cpp:1661
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr ""

#: lspclientpluginview.cpp:1685 lspclientpluginview.cpp:1697
#: lspclientpluginview.cpp:1710
#, kde-format
msgid "No Actions"
msgstr "செயல்கள் இல்லை"

#: lspclientpluginview.cpp:1701
#, kde-format
msgid "Loading..."
msgstr "ஏற்றப்படுகிறது…"

#: lspclientpluginview.cpp:1793
#, kde-format
msgid "No edits"
msgstr "திருத்தங்கள் இல்லை"

#: lspclientpluginview.cpp:1869
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr "மறுபெயரிடு"

#: lspclientpluginview.cpp:1870
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr "புதிய பெயர் (எச்சரிக்கை: சில இடங்களில் மாற்றப்படாமல் இருக்கலாம்)"

#: lspclientpluginview.cpp:1917
#, kde-format
msgid "Not enough results"
msgstr "போதுமான முடிவுகள் இல்லை"

#: lspclientpluginview.cpp:1987
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr ""

#: lspclientpluginview.cpp:2186 lspclientpluginview.cpp:2223
#, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr ""

#: lspclientpluginview.cpp:2246
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr ""

#: lspclientservermanager.cpp:597
#, kde-format
msgid "Restarting"
msgstr "மீள்துவக்கப்படுகிறது"

#: lspclientservermanager.cpp:597
#, kde-format
msgid "NOT Restarting"
msgstr "மீள்துவக்கவில்லை"

#: lspclientservermanager.cpp:598
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr ""

#: lspclientservermanager.cpp:794
#, kde-format
msgid "Failed to find server binary: %1"
msgstr "சேவையக நிரலைக் கண்டுபிடிக்க முடியவில்லை: %1"

#: lspclientservermanager.cpp:797 lspclientservermanager.cpp:839
#, kde-format
msgid "Please check your PATH for the binary"
msgstr "நிரல் உங்கள் PATH-இல் உள்ளதா என பாருங்கள்"

#: lspclientservermanager.cpp:798 lspclientservermanager.cpp:840
#, kde-format
msgid "See also %1 for installation or details"
msgstr "நிறுவல் அல்லது விவரங்களுக்கு %1 தனை பாருங்கள்"

#: lspclientservermanager.cpp:836
#, kde-format
msgid "Failed to start server: %1"
msgstr "சேவையகத்தைத் துவக்க முடியவில்லை: %1"

#: lspclientservermanager.cpp:844
#, kde-format
msgid "Started server %2: %1"
msgstr "%2 சேவையகம் துவக்கப்பட்டது: %1"

#: lspclientservermanager.cpp:878
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr ""

#: lspclientservermanager.cpp:881
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr ""

#: lspclientservermanager.cpp:885
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr ""

#: lspclientsymbolview.cpp:238
#, kde-format
msgid "Symbol Outline"
msgstr ""

#: lspclientsymbolview.cpp:276
#, kde-format
msgid "Tree Mode"
msgstr "கிளைப்படப் பயன்முறை"

#: lspclientsymbolview.cpp:278
#, kde-format
msgid "Automatically Expand Tree"
msgstr "கிளைப்படத்தைத் தானாக விரிவுப்படுத்து"

#: lspclientsymbolview.cpp:280
#, kde-format
msgid "Sort Alphabetically"
msgstr "அகர வரிசைப்படுத்து"

#: lspclientsymbolview.cpp:282
#, kde-format
msgid "Show Details"
msgstr "விவரங்களைக் காட்டு"

#: lspclientsymbolview.cpp:437
#, kde-format
msgid "Symbols"
msgstr ""

#: lspclientsymbolview.cpp:568
#, kde-format
msgid "No LSP server for this document."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:54
#, fuzzy, kde-format
#| msgid "Format on typing"
msgid "Format on save"
msgstr "தட்டச்சிடும்போதே வடிவமை"

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Enable semantic highlighting"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkInlayHint)
#: lspconfigwidget.ui:68
#, kde-format
msgid "Enable inlay hints"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:75
#, kde-format
msgid "Completions:"
msgstr "நிரப்பல்கள்:"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show inline docs for selected completion"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:89
#, kde-format
msgid "Show function signature when typing a function call"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:103
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:110
#, kde-format
msgid "Navigation:"
msgstr "உலாவல்:"

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:117
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:124
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:131
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:138
#, kde-format
msgid "Server:"
msgstr "சேவையகம்:"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:145
#, kde-format
msgid "Show program diagnostics"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:152
#, kde-format
msgid "Show notifications from the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:159
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:166
#, kde-format
msgid "Document outline:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:173
#, kde-format
msgid "Sort symbols alphabetically"
msgstr "குறியீட்டுகளை அகரவரிசையாக காட்டு"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:180
#, kde-format
msgid "Display additional details for symbols"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:187
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:202
#, kde-format
msgid "Automatically expand tree"
msgstr "கிளைப்படத்தைத் தானாக விரிவுப்படுத்து"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:237
#, kde-format
msgid "User Server Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:245
#, kde-format
msgid "Settings File:"
msgstr "அமைப்புக்கோப்பு"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:272
#, kde-format
msgid "Default Server Settings"
msgstr ""

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, kde-format
msgid "More Options"
msgstr "மேலும் விருப்பங்கள்"

#~ msgid "Quick Fix"
#~ msgstr "விரை திருத்தம்"

#~ msgid "Error"
#~ msgstr "சிக்கல்"

#~ msgid "Warning"
#~ msgstr "எச்சரிக்கை"

#~ msgid "Information"
#~ msgstr "தகவல்"

#~ msgctxt "@info"
#~ msgid ""
#~ "Error in regular expression: %1\n"
#~ "offset %2: %3"
#~ msgstr ""
#~ "சுருங்குறித் தொடரில் சிக்கல்: %1\n"
#~ "எழுத்தெண் %2: %3"

#~ msgid "Copy to Clipboard"
#~ msgstr "பிடிப்புப்பலகைக்கு நகலெடு"
